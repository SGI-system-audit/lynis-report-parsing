# import libraries

import pandas as pd
from pandas import ExcelWriter
from openpyxl import load_workbook
import os


def append_df_to_excel(filename, df, sheet_name='Sheet1', startrow=None, truncate_sheet=False, **to_excel_kwargs):
    """
    Append a DataFrame [df] to existing Excel file [filename]
    into [sheet_name] Sheet.
    If [filename] doesn't exist, then this function will create it.

    @param filename: File path or existing ExcelWriter
                     (Example: '/path/to/file.xlsx')
    @param df: DataFrame to save to workbook
    @param sheet_name: Name of sheet which will contain DataFrame.
                       (default: 'Sheet1')
    @param startrow: upper left cell row to dump data frame.
                     Per default (startrow=None) calculate the last row
                     in the existing DF and write to the next row...
    @param truncate_sheet: truncate (remove and recreate) [sheet_name]
                           before writing DataFrame to Excel file
    @param to_excel_kwargs: arguments which will be passed to `DataFrame.to_excel()`
                            [can be a dictionary]
    @return: None

    Usage examples:

    >>> append_df_to_excel('d:/temp/test.xlsx', df)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, header=None, index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2',
                           index=False)

    >>> append_df_to_excel('d:/temp/test.xlsx', df, sheet_name='Sheet2', 
                           index=False, startrow=25)

    (c) [MaxU](https://stackoverflow.com/users/5741205/maxu?tab=profile)
    """
    # Excel file doesn't exist - saving and exiting
    if not os.path.isfile(filename):
        df.to_excel(
            filename,
            sheet_name=sheet_name, 
            startrow=startrow if startrow is not None else 0, 
            **to_excel_kwargs)
        return
    
    # ignore [engine] parameter if it was passed
    if 'engine' in to_excel_kwargs:
        to_excel_kwargs.pop('engine')

    writer = pd.ExcelWriter(filename, engine='openpyxl', if_sheet_exists='replace', mode='a')

    # try to open an existing workbook
    writer.book = load_workbook(filename)
    
    # get the last row in the existing Excel sheet
    # if it was not specified explicitly
    if startrow is None and sheet_name in writer.book.sheetnames:
        startrow = writer.book[sheet_name].max_row

    # truncate sheet
    if truncate_sheet and sheet_name in writer.book.sheetnames:
        # index of [sheet_name] sheet
        idx = writer.book.sheetnames.index(sheet_name)
        # remove [sheet_name]
        writer.book.remove(writer.book.worksheets[idx])
        # create an empty sheet [sheet_name] using old index
        writer.book.create_sheet(sheet_name, idx)
    
    # copy existing sheets
    writer.sheets = {ws.title:ws for ws in writer.book.worksheets}

    if startrow is None:
        startrow = 0

    # write out the new sheet
    df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)

    # save the workbook
    writer.save()

# function to get data.
def get_data():
	warnings = open('warnings.txt', 'r')
	suggestions = open('suggestions.txt', 'r')
	packages = open('packages.txt', 'r')
	shells = open('shells.txt', 'r')
	warn_data = warnings.readlines()
	sugg_data = suggestions.readlines()
	pack_data = packages.read()
	shell_data = shells.readlines()
	return warn_data, sugg_data, pack_data, shell_data

def clean_data():
	warn, sugg, pack, shell = get_data()
	
	#Process Warning section
	warn_clean = []
	for line in warn:
		warn_clean.append(line.split('|'))
	for i in range( len(warn_clean)):
		warn_clean [i] = warn_clean [i][:2]
	#print(warn_clean[i])
	
	#Process Suggestion section
	sugg_clean = [] 
	for line in sugg:
		sugg_clean.append(line.split('|'))
	for i in range( len(sugg_clean)):
		sugg_clean[i] = sugg_clean[i][:2]
	#print(sugg_clean[i])
	
	#Process Installed Packages section
	pack_clean = []
	pack = pack.split('|')
	pack_clean = pack
	del pack_clean [0]
	
	#Process Available Shell section
	shell_clean = []
	for i in range(len(shell)):
		shell_clean.append(shell[i].rstrip('')[:-1])
	#print(shell_clean[i])
	
	return warn_clean, sugg_clean, pack_clean, shell_clean

def convert_to_excel():
	warnings, suggestions, packages, shells = clean_data()

	warn_code = []
	warn_text = []
	for i in range(len(warnings)):
		warn_code.append(warnings[i][0])
	for i in range(len(warnings)):
		warn_text.append(warnings[i][1])
	#print(warn_packages, warn_text)
	warn = pd.DataFrame()
	warn['Code'] = warn_code
	warn['Warning'] = warn_text
	append_df_to_excel('warnings.xlsx', warn, sheet_name='warning')

	sugg_code = []
	sugg_text = []
	for i in range(len(suggestions)):
		sugg_code.append(suggestions[i][0])
	for i in range( len(suggestions)):
		sugg_text.append(suggestions [i][1])
	#print(sugg_code, sugg_text)
	sugg = pd.DataFrame()
	sugg['Code'] = sugg_code
	sugg['Suggestion'] = sugg_text
	append_df_to_excel('warnings.xlsx', sugg, sheet_name='suggestion')
	
	pack_data = pd.DataFrame()
	pack_data['Packages'] = packages
	append_df_to_excel('warnings.xlsx', pack_data, sheet_name='package')

	shell_data = pd.DataFrame()
	shell_data['Shells'] = shells
	append_df_to_excel('warnings.xlsx', shell_data, sheet_name='shell')

if __name__ == "__main__":
	convert_to_excel()
	print("XLS file is ready")
